import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppPost, AppLogin } from '../interfaces/interfaces';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TestService {
  apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {}

  getPosts() {
    return this.http.get<AppPost>('https://jsonplaceholder.typicode.com/posts');
  }

  login(body: AppLogin) {
    return this.http.post(`${this.apiUrl}/login`, body);
  }
}
