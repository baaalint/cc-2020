export interface AppPost {
  body: string;
  id: number;
  title: string;
  userId: number;
}

export interface AppLogin {
  userEmail: string;
  userPassword: string;
}
