import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { generateRandomColor } from './utils/utils';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  testPage = 'test';
  constructor(private router: Router) {}
}
