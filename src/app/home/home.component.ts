import { Component, OnInit } from '@angular/core';
import { generateRandomColor } from '../utils/utils';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  constructor() {}
  color: string;
  bgColor: string;

  ngOnInit() {}

  randomColor() {
    this.color = generateRandomColor();
  }

  changeHomeBgColor(color: string) {
    this.bgColor = color;
  }
}
