import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TestService } from '../services/test.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private fb: FormBuilder, private testService: TestService) {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      userEmail: ['', [Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'), Validators.required]],
      userPassword: ['', [Validators.required, Validators.minLength(8)]],
    });
  }

  onSubmit() {
    this.testService
      .login({
        ...this.loginForm.value,
      })
      .pipe(take(1))
      .subscribe(console.log);
    console.log(this.loginForm);
  }
}
