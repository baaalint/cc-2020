import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { generateRandomColor } from '../utils/utils';
import { TestService } from '../services/test.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss'],
})
export class TestComponent implements OnInit {
  @Input() bgColor = '#000';
  @Output() homeBgColor: EventEmitter<string> = new EventEmitter();
  postArray$ = this.testService.getPosts();
  constructor(private testService: TestService) {}

  ngOnInit() {}

  changeHomeBackground() {
    this.homeBgColor.emit(generateRandomColor());
  }
}
